<?php

/**
 * @file
 * Define pages for Forum Grade Center and Peer Review.
 */

/**
 * Create and then download a spreadsheet of information presented
 */
function class_participation_csv_summary($gid, $display = 'average') {
  /*** Rename the page with course name and the type of data displayed ***/
  $course_title = db_query("SELECT title FROM {node} WHERE nid = :gid", array(':gid' => $gid))->fetchField();

  $download_date = date('m-d-Y');
  $csvfile = 'class_participation_' . $gid . '_' . $download_date . '.csv';
  header('Content-Type: text/csv; charset=utf-8');
  header('Content-Disposition: attachment; filename="' . $csvfile . '"');

  // create a file pointer connected to the output stream
  $output = fopen('php://output', 'w');

  // Print out title to csv.
  fputcsv($output, array($course_title));

  if ($display == 'average') {
    $header = array(
      'Name',
      'Average Grade',
      'date'
    );
  } else {
    $header = array(
      'Name',
      'Grade',
    );
  }


  fputcsv($output, $header);


  $max_value_var = NULL;
  $max_value_var = variable_get('max_value_' . arg(1), '');
  if(!$max_value_var) {
    variable_set('max_value_' . arg(1), 100);
    variable_set('starting_value_' . arg(1), 0);
  }
  $node = node_load(arg(1));

  $result = db_query("SELECT cp.uid, cp.grade, cp.timestamp, CONCAT(fdfn.field_first_name_value, ' ',
      fdln.field_last_name_value) as fullname FROM class_participation cp,
      field_data_field_first_name fdfn, field_data_field_last_name fdln, users u, file_managed fm WHERE cp.gid = :gid
      and fdln.entity_id = cp.uid and fdfn.entity_id = cp.uid and u.uid = cp.uid and fm.fid = u.picture order by fdln.field_last_name_value, fdfn.field_first_name_value",
    array('gid' => arg(1)));

  foreach($result as $row) {
    if(!isset($user_totals[$row->uid]['total'])) {
      $user_totals[$row->uid]['total'] = 0;
    }
    if(!isset($user_totals[$row->uid]['count'])) {
      $user_totals[$row->uid]['count'] = 0;
    }
    $user_totals[$row->uid]['fullname'] = $row->fullname;
    $user_totals[$row->uid]['uri'] = $row->uri;
    $user_totals[$row->uid]['total'] = $user_totals[$row->uid]['total'] + $row->grade;
    $user_totals[$row->uid]['count'] = $user_totals[$row->uid]['count'] + 1;

    $array_row = array($row->fullname, $row->grade, date('d/m/Y',$row->timestamp));
    if ($display == 'average') {
      fputcsv($output, $array_row);
    }

    //$row->fullname
    //$row->grade
    //l(date('m/d/Y', $row->timestamp), 'cp/' . arg(1) . '/class-participation/' . $row->timestamp)

    $count++;
  }

  // Learning Groups Grades.
  $user_totals_lg = array();
  $result = db_query('SELECT cp.lgid, cp.grade, cp.comment, n.title, cp.timestamp from class_participation cp, node n where n.nid = cp.lgid
    and gid = :gid and lgid is not null',
    array('gid' => arg(1)));
  $count = 1;
  foreach($result as $row) {
    if(!isset($user_totals_lg[$row->lgid]['total'])) {
      $user_totals_lg[$row->lgid]['total'] = 0;
    }
    if(!isset($user_totals_lg[$row->lgid]['count'])) {
      $user_totals_lg[$row->lgid]['count'] = 0;
    }
    $user_totals_lg[$row->lgid]['title'] = $row->title;
    $user_totals_lg[$row->lgid]['total'] = $user_totals_lg[$row->lgid]['total'] + $row->grade;
    $user_totals_lg[$row->lgid]['count'] = $user_totals_lg[$row->lgid]['count'] + 1;

    $count++;
  }

  foreach($user_totals as $row2) {
    if(!isset($row->fullname)) {
      $row->fullname = '';
    }
    if(!isset($row->uri)) {
      $row->uri = '';
    }
    if($row2['total'] == 0) {
      $average_grade = 0;
    }
    else {
      $average_grade = $row2['total']/$row2['count'];
    }
    $array_row = array($row->title, $user_totals_lg[$row->lgid]['total'], $user_totals_lg[$row->lgid]['count']);
    if ($display != 'average') {
      fputcsv($output, $array_row);
    }

    $count++;
  }
  fclose($output);



}
