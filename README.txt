INTRODUCTION
------------
Welcome to the Class Participation module.  The module works with organic groups and allows you to create any number of sessions to grade the group members,
(class members).  The slider ui is used to grade students.

REQUIREMENTS
------------
og

INSTALLATION
------------
1.)  Enable the class participation module.
2.)  To create a new session go to node/gid/class-participation and replace gid with the group id number (node number of the organic group)
3.)  To view the reporting page, and the list of already entered grades go to node/gid/class-participation-list-page
4.)  The module has been tested with groups with a machine name of "group" (english name of Group), and learning groups (teams of people who work together for
  a common grade) with a machine name of "learning_group" (english name of Learning Group).