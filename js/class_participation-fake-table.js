(function($) {
  $(document).ready(function(){
    /*** theme fake table js ***/
    $('.fake-table-expand-collapse').click(function() {
      $(this).toggleClass('collapse');
      $(this).parent().siblings('.fake-table').slideToggle('slow', function() {
        $(this).removeAttr('style').toggleClass('collapsed');
      });
      $(this).parent().siblings('.fake-table-name').children('.fake-table-name-button-wrapper').fadeToggle('slow',function() {
        $(this).removeAttr('style').toggleClass('collapsed');
      });
    });
  });
}(jQuery));