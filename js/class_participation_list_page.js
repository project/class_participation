(function($) {
    $(document).ready(function(){
      var gid = Drupal.settings.class_participation.cp_gid;
      var jsonForCsvAll = Drupal.settings.class_participation.json_for_csv_all;

      var jsonForCsvIndividuals = Drupal.settings.class_participation.json_for_csv_individual;
      var jsonForCsvGroups = Drupal.settings.class_participation.json_for_csv_group;
      $('.view-individual-sessions-text').hide();
      $('.view-group-sessions-text').hide();
      $('.fake-table-expand-collapse-wrapper').click(function (){
        $(this).find('.fake-data-table').toggleClass('collapsed');
      });
      $('.lg-image-hover').mouseover(function(){
        $(this).find('.lg-row').css('display', 'block');
      });
      $('.lg-image-hover').mouseleave(function(){
        $(this).find('.lg-row').css('display', 'none');
      });

      $('.cp-list-page-button').click(function() {
        $( ".fake-table-wrapper").show();
        $('.cp-list-page-button').removeClass('cp-list-page-button-selected');
        $(this).toggleClass('cp-list-page-button-selected');

        if($(this).attr('id') == 'view_individual_sessions') {
            $('.discussion-lgname').parent().parent().parent().hide();
            $('.discussion-name').parent().show();
            $('.view-all-sessions-text').hide();
            $('.view-individual-sessions-text').show();
            $('.view-group-sessions-text').hide();
            return;
        }
        else {
            $('.discussion-name').parent().show();
        }
        if($(this).attr('id') == 'view_group_sessions') {
            $('.view-group-sessions-text').show();
            $('.view-individual-sessions-text').hide();
            $('.view-all-sessions-text').hide();
            $('.discussion-name').parent().hide();
            $('.fake-thead').show();
            $( ".fake-table-wrapper" ).each(function( index ) {
                var numItems = $(this).find('.discussion-lgname').length;
                if(numItems < 1) {
                    $(this).hide();
                }
            });
        }
        else {
            $('.discussion-name').parent().show();
            $('.view-group-sessions-text').hide();
            $('.view-individual-sessions-text').hide();
            $('.view-all-sessions-text').show();
        }
      });
      $('#by-students').click(function() {
        window.location = '/node/' + gid + '/class-participation-list-page-by-author';
      });
      $('#by-sessions').click(function() {
        window.location = '/node/' + gid + '/class-participation-list-page';
      });
      $('#edit-selected').change(function () {
        var uid = this.value;
        window.location = '/node/' + gid + '/class-participation-list-page-by-author/' + uid;
      });
      $('.left-button').click(function () {
        var prevVal = $('#edit-selected option:selected').prev().val();
        window.location = '/node/' + gid + '/class-participation-list-page-by-author/' + prevVal;
      });
      $('.right-button').click(function () {
        var nextVal = $('#edit-selected option:selected').next().val();
        window.location = '/node/' + gid + '/class-participation-list-page-by-author/' + nextVal;
      });


        $('#csv-button').click(function(e) {
          e.preventDefault();
          var selectedBlueButton = $('.cp-list-page-button-selected').text();

          // JSON must be in the following format to output to csv
          /*var items = [
              { name: "Item 1", color: "Green", size: "X-Large" },
              { name: "Item 2", color: "Green", size: "X-Large" },
              { name: "Item 3", color: "Green", size: "X-Large" }];*/

          if(selectedBlueButton == 'VIEW ALL SESSIONS') {
            JSONToCSVConvertor(jsonForCsvAll, 'Class Participation Data', false);
          }
          if(selectedBlueButton == 'VIEW INDIVIDUAL SESSIONS') {
            JSONToCSVConvertor(jsonForCsvIndividuals, 'Class Participation Data', false);
          }
          if(selectedBlueButton == 'VIEW GROUP SESSIONS') {
            JSONToCSVConvertor(jsonForCsvGroups, 'Class Participation Data', false);
          }
      });
      $('#print-button').click(function (e) {
         e.preventDefault();
         window.print();
      });

    });

    /*function DownloadJSON2CSV(objArray)
    {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if(line != '') line += ','

                line += array[i][index];
            }

            str += line + '\r\n';
        }

        if (navigator.appName != 'Microsoft Internet Explorer')
        {
            window.open('data:text/csv;charset=utf-8,' + escape(str));
        }
        else
        {
            var popup = window.open('','csv','');
            popup.document.body.innerHTML = '<pre>' + str + '</pre>';
        }
    }*/

    function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

        var CSV = '';
        //Set Report title in first row or line
        if(ShowLabel) {
          CSV += ReportTitle + '\r\n\n';
        }

        //This condition will generate the Label/Header
        if (ShowLabel) {
            var row = "";

            //This loop will extract the label from 1st index of on array
            for (var index in arrData[0]) {

                //Now convert each value to string and comma-seprated
                row += index + ',';
            }

            row = row.slice(0, -1);

            //append Label row with line break
            CSV += row + '\r\n';
        }

        //1st loop is to extract each row
        for (var i = 0; i < arrData.length; i++) {
            var row = "";

            //2nd loop will extract each column and convert it in string comma-seprated
            for (var index in arrData[i]) {
                row += '"' + arrData[i][index] + '",';
            }

            row.slice(0, row.length - 1);

            //add a line break after each row
            CSV += row + '\r\n';
        }

        if (CSV == '') {
            alert("Invalid data");
            return;
        }

        //Generate a file name
        var fileName = "MyReport_";
        //this will remove the blank-spaces from the title and replace it with an underscore
        fileName += ReportTitle.replace(/ /g,"_");

        //Initialize file format you want csv or xls
        var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension

        //this trick will generate a temp <a /> tag
        var link = document.createElement("a");
        link.href = uri;

        //set the visibility hidden so it will not effect on your web-layout
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";

        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}(jQuery));