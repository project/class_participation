(function($) {
    $(document).ready(function(){
        var theFormId = null;
        var unixTimeUrl = null;
        var uniqueFormId = null;
        var classParticipationSettings = Drupal.settings.class_participation.theSettings;
        var gid = Drupal.settings.class_participation.cp_gid;
        var default_initial_value = Drupal.settings.class_participation.initial_value;
        var default_max_value = Drupal.settings.class_participation.default_max_value;
        var default_min_value = Drupal.settings.class_participation.default_min_value;

        // Naturally Drupal will put the current value in this field, but our peeps want the
        // default minimum value here.
        $('.sliderfield-display-values-field').addClass('default-min-value').removeClass('sliderfield-display-values-field');
        $('.default-min-value').text(default_min_value);

        obj = JSON.parse(classParticipationSettings);
        $('#edit-date-value').datepicker({
        });

        $('.lg-image-hover').mouseover(function(){
            $(this).find('.lg-row').css('display', 'block');
        });
        $('.lg-image-hover').mouseleave(function(){
            $(this).find('.lg-row').css('display', 'none');
        });

        $('.sliderfield-container').each(function (i, obj) {
           var sliderVal = $(this).find('.sliderfield-bubble').text();
           if (Number(sliderVal) < Number(default_initial_value)) {
               $(this).find('.sliderfield-bubble').css('background-color', '#F66C6C');
               $(this).find('.sliderfield-bubble').css('color', '#F6F6F6');
           }
           else if (Number(sliderVal) == Number(default_initial_value)) {
               $(this).find('.sliderfield-bubble').css('background-color', 'black');
               $(this).find('.sliderfield-bubble').css('color', '#F6F6F6');
           }
           else {
               $(this).find('.sliderfield-bubble').css('background-color', '#006546');
               $(this).find('.sliderfield-bubble').css('color', '#F6F6F6');
           }
        });

        $('.sliderfield').mouseup(function () {
           var sliderVal = $(this).find('.sliderfield-bubble').text();
           if (Number(sliderVal) < Number(default_initial_value)) {
               $(this).find('.sliderfield-bubble').css('background-color', '#F66C6C');
               $(this).find('.sliderfield-bubble').css('color', '#F6F6F6');
           }
           else if (Number(sliderVal) == Number(default_initial_value)) {
               $(this).find('.sliderfield-bubble').css('background-color', 'black');
               $(this).find('.sliderfield-bubble').css('color', '#F6F6F6');
           }
           else {
               $(this).find('.sliderfield-bubble').css('background-color', '#006546');
               $(this).find('.sliderfield-bubble').css('color', '#F6F6F6');
           }
        });

        $('#cp-goto-button').click(function() {
           var radioVal = $("input:radio[name=learning-group-or-individual]:checked").val();
           if (radioVal == 1) {
             window.location =  '/node/' + gid + '/class-participation-enter-grades-learning-groups/' + theFormId;
           }
           else {
              window.location =  '/node/' + gid + '/class-participation-enter-grades/' + theFormId;
           }
        });
        $('#cp-edit-config-button').click(function () {
            $('#class-participation-custom-form-config2').show();
        });

        $('#close-button').click(function () {
            $('#cp-goto-button').addClass('hidden');
            $('#cp-edit-config-button').addClass('hidden');
            $('#class-participation-custom-form-config2').hide();
            $('#class-participation-selector').val('-----');

        });

        $('#class-participation-selector').change(function() {
            $('#error-message').text('');
            if($(this).attr('value') == 'create-new') {
              $('#edit-date-value').val('');
              $("#class-participation-custom-form-config2 input:radio").attr('disabled', false);
              $('#edit-delete').hide();
              $('#cp-goto-button').addClass('hidden');
              $('#cp-edit-config-button').addClass('hidden');
              $('#class-participation-custom-form-config2').show();
              $('#edit-title-value').val("");
              $('#edit-minimum-value').val("");
              $('#edit-initial-value').val("");
              $('#edit-maximum-value').val("");
              $('#edit-unique-form-id-value').val("");
              $('#edit-class-date').val("");
              $('#edit-submit').val('Create new session');

            }
            else if ($(this).attr('value') == '-----') {
              $("#class-participation-custom-form-config2 input:radio").attr('disabled',true);
              $('#cp-goto-button').addClass('hidden');
              $('#cp-edit-config-button').addClass('hidden');
              $('#class-participation-custom-form-config2').hide();
              $('#edit-submit').val('Submit');
            }
            else {
              $("input:radio").attr('disabled',true);
              $('#edit-delete').show();
              $('#cp-goto-button').removeClass('hidden');
              $('#cp-edit-config-button').removeClass('hidden');
              $('#edit-submit').val('Submit');
              var uniqueFormId = $(this).attr('value');
              for (var key in obj) {
                  var patt = new RegExp(".*?survey_type.*?" + uniqueFormId + "$");
                  var res = patt.test(key);
                  if(res) {
                      $("input[name='learning-group-or-individual'][value='" + obj[key] + "']").attr('checked', true);

                  }
                  var patt = new RegExp(".*?title_value.*?" + uniqueFormId + "$");
                  var res = patt.test(key);
                  if(res) {
                      $('#edit-title-value').val(obj[key]);
                  }
                  var patt = new RegExp(".*?min_value.*?" + uniqueFormId + "$");
                  var res = patt.test(key);
                  if(res) {
                      $('#edit-minimum-value').val(obj[key]);
                  }
                  var patt = new RegExp(".*?starting_value.*?" + uniqueFormId + "$");
                  var res = patt.test(key);
                  if(res) {
                      $('#edit-initial-value').val(obj[key]);
                  }
                  var patt = new RegExp(".*?max_value.*?" + uniqueFormId + "$");
                  var res = patt.test(key);
                  if(res) {
                      $('#edit-maximum-value').val(obj[key]);

                  }
                  var patt = new RegExp(".*?unique_survey_id.*?" + uniqueFormId + "$");
                  var res = patt.test(key);
                  if(res) {
                      $('#edit-unique-form-id-value').val(obj[key]);
                      uniqueFormId = obj[key];
                  }
                  var patt = new RegExp(".*?date.*?" + uniqueFormId + "$");
                  var res = patt.test(key);
                  var formattedDate = new Date(obj[key] * 1000);
                  var month = formattedDate.getMonth() + 1;
                  var day = formattedDate.getDate();
                  var year = formattedDate.getFullYear();
                  formattedDate = month + '/' + day + '/' + year;
                  if(res) {
                      $('#edit-date-value').val(formattedDate);
                      theFormId = uniqueFormId;
                      unixTimeUrl = new Date(formattedDate).getTime() / 1000;
                  }

              }
            }
        });
        $('input:radio[name=learning-group-or-individual]').change(function() {
           // 0 is non learning group, 1 is learning group radio button
           if($(this).val() == 1) {
             $('.learning-group-grades').removeClass('hidden');
             $('.individual-grades').addClass('hidden');
           }
           else {
             $('.learning-group-grades').addClass('hidden');
             $('.individual-grades').removeClass('hidden');
           }
        });

        $('#class-participation-custom-form-config2 #edit-submit').click(function (e){
            $('#error-message').text('');
            var minimumValue = parseInt($('#edit-minimum-value').val());
            var maximumValue = parseInt($('#edit-maximum-value').val());
            var initialValue = parseInt($('#edit-initial-value').val());
            var editTitleValue = $('#edit-title-value').val();
            var editDateValue = $('#edit-date-value').val();
            if(editTitleValue.length > 65) {
                $('#error-message').append('The title length must be less than 65 characters long.  <br/>');
                e.preventDefault(); //prevent the default action
            }
            var titleVal = $('#edit-title-value').val();
            if(titleVal.trim() == '') {
              $('#error-message').append('The title field is required <br/>');
              e.preventDefault(); //prevent the default action
            }
            if((!initialValue && initialValue !=0) || (!maximumValue && maximumValue != 0) || (!minimumValue && minimumValue !=0)
                || (!editDateValue && editDateValue != 0) || (!editTitleValue && editTitleValue !=0)) {
                $('#error-message').append('All values must be set to submit the form. <br/>');
                e.preventDefault(); //prevent the default action
            }
            if(initialValue < minimumValue) {
                $('#error-message').append('The initial value must be greater than the minimum value.  <br/>');
                e.preventDefault(); //prevent the default action
            }

            if(minimumValue >= maximumValue) {
                $('#error-message').append('The minimum value must be less than the maximum value.  <br/>');
                e.preventDefault(); //prevent the default action
            }

            if(maximumValue <= minimumValue) {
                $('#error-message').append('The maximum value must be greater than the minimum value.  <br/>');
                e.preventDefault(); //prevent the default action
            }
            if(initialValue > maximumValue) {
                $('#error-message').append('The initial value must be less than the maximum value.  <br/>');
                e.preventDefault(); //prevent the default action
            }

            if(isNaN(minimumValue)) {
                $('#error-message').append('The minimum value must be a number.  <br/>');
                e.preventDefault(); //prevent the default action
            }
            if(isNaN(maximumValue)) {
                $('#error-message').append('The maximum value must be a number.  <br/>');
                e.preventDefault(); //prevent the default action
            }

            if($('#edit-maximum-value').val().length > 3) {
                $('#error-message').append('The maximum value must be less than 3 digits long.  <br/>');
                e.preventDefault(); //prevent the default action
            }

            if($('#edit-minimum-value').val().length > 3) {
                $('#error-message').append('The minimum value must be less than 3 digits long.  <br/>');
                e.preventDefault(); //prevent the default action
            }

            if($('#edit-initial-value').val().length > 3) {
                $('#error-message').append('The initial value must be less than 3 digits long.  <br/>');
                e.preventDefault(); //prevent the default action
            }

            if($('#edit-minimum-value').val().indexOf('.') !== -1) {
                $('#error-message').append('The minimum value cannot contain decimals <br/>');
                e.preventDefault(); //prevent the default action
            }

            if($('#edit-maximum-value').val().indexOf('.') !== -1) {
                $('#error-message').append('The maximum value cannot contain decimals <br/>');
                e.preventDefault(); //prevent the default action
            }

            if($('#edit-initial-value').val().indexOf('.') !== -1) {
                $('#error-message').append('The initial value cannot contain decimals <br/>');
                e.preventDefault(); //prevent the default action
            }

            var isAllNum1 = is_int(($('#edit-minimum-value').val()));
            var isAllNum2 = is_int($('#edit-maximum-value').val());
            var isAllNum3 = is_int(($('#edit-initial-value').val()));
            if(!isAllNum1 || !isAllNum2 || !isAllNum3) {
                $('#error-message').append('The values must be numeric and not contain alpha characters<br/>');
                e.preventDefault(); //prevent the default action
            }


            if(isNaN(initialValue)) {
                $('#error-message').append('The initial value must be a number.  <br/>');
                e.preventDefault(); //prevent the default action
            }
        });

    });
}(jQuery));

function is_int(value){
    if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
        return true;
    } else {
        return false;
    }
}
