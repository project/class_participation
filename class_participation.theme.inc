<?php

/**
 * @file
 * Define pages for Forum Grade Center Forum Summary
 */

/**
 * Called by hook theme to display a table like display of information presented
 */

function theme_class_participation_fake_table($variables) {
  drupal_add_css(drupal_get_path('module', 'class_participation') . '/css/fake-table-sass.css');
  drupal_add_js(drupal_get_path('module', 'class_participation') . '/js/class_participation-fake-table.js', array('scope' => 'footer'));
  $fake_table_name = $variables['fake_table_name'];
  $header = $variables['header'];
  $rows = $variables['rows'];
  $attributes = $variables['attributes'];
  $sticky = $variables['sticky'];
  $expand_collapse = $variables['expand_collapse'];

  $expand_icon = '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 20 20" enable-background="new 0 0 20 20" xml:space="preserve">
<line class ="vert-line" fill="none" stroke="#fff" stroke-width="3" x1="10" y1="14.2" x2="10" y2="5.8"/>
<circle fill="none" stroke="#fff" stroke-width="2" cx="10" cy="10" r="8.5"/>
<line class ="horiz-line" fill="none" stroke="#fff" stroke-width="3" x1="5.7" y1="10" x2="14.2" y2="10"/>
</svg>';

  // Wrap theme elements for easier jQuery selection
  $output = "<div class='fake-table-wrapper no-print'>";
  // Add fake table header with title
  if (count($fake_table_name)) {
    if (!is_null($expand_collapse)) {
      $output .= '<div class= "fake-table-name expand-collapse">' . $fake_table_name . '</div>';
      $output .= '<div class="fake-table-expand-collapse-wrapper">';
      if ($expand_collapse == 'expanded') {
        $output .=   '<span class="fake-table-expand-collapse collapse">' . $expand_icon . '</span>';
      } else {
        $output .=   '<span class="fake-table-expand-collapse">' . $expand_icon . '</span>';
      }

      $output .= '</div><!-- .fake-table-expand-collapse -->';
    } else {
      $output .= '<div class= "fake-table-name">' . $fake_table_name . '</div>';
    }

  }
  // Add sticky headers, if applicable.
  if (count($header) && $sticky) {
    drupal_add_js('misc/tableheader.js');
    // Add 'sticky-enabled' class to the table to identify it for JS.
    // This is needed to target tables constructed by this function.
    $attributes['class'][] = 'sticky-enabled';
  }
  $attributes['class'][] = 'fake-table';
  if ($expand_collapse == 'collapsed') {
    $attributes['class'][] = 'collapsed';
  }

  $output .= '<div' . drupal_attributes($attributes) . ">\n";

  // Format the fake table header:
  if (count($header)) {
    $output .= (count($rows) ? ' <div class="fake-thead has-rows">' : ' <dive class"fake-thead no-rows">');
    if (is_array($header)) {
      foreach ($header as $cell) {
//      $cell = tablesort_header($cell, $header, $ts);
        $output .= _theme_class_participation_forum_topic_cell($cell, TRUE);
      }
    } else {
      $output .= $header;
    }
    // Using ternary operator to close the tags based on whether or not there are rows
    $output .= "</div><!-- .fake-thead -->\n";
  }


  // Format the fake table rows:
  if (count($rows)) {

    $flip = array(
      'even' => 'odd',
      'odd' => 'even',
    );
    $class = 'even';
    $cell_count = 0;
    foreach ($rows as $number => $row) {
      // special cases handling thanks to php.net/strops
      $header_row_class = FALSE; // most rows aren't headers
      $subhead_row_class = FALSE; // most rows aren't subheaders either
      $cell_row_class = FALSE; // most rows don't have just 1 cell
      $class_string = $row[0]['class'];
      $header_row_class_string = "header-row";
      $subhead_row_class_string = "subhead-row";
      $cell_row_class_string = "cell-row";
      $header_row_pos = strpos($class_string, $header_row_class_string);
      $subhead_row_pos = strpos($class_string, $subhead_row_class_string);
      $cell_row_pos = strpos($class_string, $cell_row_class_string);

      if ($header_row_pos != FALSE) {
        $header_row_class = TRUE;
      }
      if ($subhead_row_pos != FALSE) {
        $subhead_row_class = TRUE;
      }
      if ($cell_row_pos != FALSE) {
        $cell_row_class = TRUE;
      }
      // Check if we're dealing with a simple or complex row
      if (isset($row['data'])) {
        $cells = $row['data'];
        $no_striping = isset($row['no_striping']) ? $row['no_striping'] : FALSE;
        // Set the attributes array and exclude 'data' and 'no_striping'.
        $attributes = $row;
        unset($attributes['data']);
        unset($attributes['no_striping']);
      }
      else {
        $cells = $row;
        $attributes = array();
        $no_striping = FALSE;
      }

      if (count($cells)) {
        // Add odd/even class
        if (!$no_striping) {
          $class = $flip[$class];
          $attributes['class'][] = $class . ' fake-row-wrapper cell-count-' . $cell_count;
        } else {
          $attributes['class'][] = 'fake-row-wrapper cell-count-' . $cell_count;
        }
        if ($header_row_class == TRUE) {
          $attributes['class'][] = $header_row_class_string;
          // reset
          $header_row_class == FALSE;
        }
        if ($subhead_row_class == TRUE) {
          $attributes['class'][] = $subhead_row_class_string;
          // reset
          $subhead_row_class == FALSE;
        }
        if ($cell_row_class == TRUE) {
          $attributes['class'][] = $cell_row_class_string;
          // reset
          $cell_row_class == FALSE;
        }

        // Build row
        $output .= ' <div' . drupal_attributes($attributes) . ' class="fake-row">';
        $i = 0;
        foreach ($cells as $cell) {
//          $cell = tablesort_cell($cell, $header, $ts, $i++);
          $output .= _theme_class_participation_forum_topic_cell($cell);
        }
        $output .= " </div><!-- .fake-row -->\n";
      }
      $cell_count++;
    }

  }

  $output .= "</div><!-- .fake-table -->\n";
  $output .= "</div><!-- .fake-table-wrapper-->\n";
  return $output;
}


function _theme_class_participation_forum_topic_cell($cell, $header = FALSE) {
  if (is_array($cell)) {
    $data = isset($cell['data']) ? $cell['data'] : '';
    // Cell's data property can be a string or a renderable array.
    if (is_array($data)) {
      $data = drupal_render($data);
    }
    $header |= isset($cell['header']);
    unset($cell['data']);
    unset($cell['header']);
    $attributes = drupal_attributes($cell);
  }
  else {
    $data = $cell;
  }

  if ($header) {
    $output = "<div$attributes>$data</div>";
  }
  else {
    $output = "<div$attributes>$data</div>";
  }

  return $output;
}

/**
 * Call for presentation layer of class-participation-list-page.
 */

function theme_class_participation_list_page($variables) {
  $cp_gid = array('cp_gid' => arg(1));
  drupal_add_js(array ("class_participation" => $cp_gid), 'setting');
  $path = drupal_get_path('module', 'class_participation');
  drupal_add_css($path . '/css/class_part_list_page.css');
  drupal_add_css("$path/css/class_participation.css");
  drupal_add_js("$path/js/class_participation_list_page.js", array('scope' => 'footer'));
  $content =  '<div class="report-types"><span class="report-type underlined" id="by-sessions">By Sessions</span>
    <span class="report-type" id="by-students">By Students</span></div>';
  $node = node_load(arg(1));
  drupal_set_title($node->title . ' : Class Participation');

  $header = array(
    array(
      'data' => t('Name'),
      'class' => 'fake-table discussion-name'
    ),
    array(
      'data' => t('Score'),
      'class' => 'fake-table total-replies grand-total-grade grade-data',
      'data-hide' => 'phone,tablet'
    ),
    array(
      'data' => t('Comment'),
      'class' => 'fake-table total-replies grand-total-grade grade-data',
      'data-hide' => 'phone,tablet'
    ),
  );
  $rows[] = array(
    array(
      'data' => 'TOTALS:',
      'class' => 'fake-table discussion-name'
    ),
    array(
      'data' =>
        "<span class = 'grand-total-grade'>" .
        1 .
        "</span>",
      'class' => 'fake-table total-replies grade-data'
    ),
    array(
      'data' => 3,
      'class' => 'fake-table total-replies grand-total-grade grade-data',

    ),
    array(
      'data' => 7,
      'class' => 'fake-table total-replies grand-total-grade grade-data',

    ),
  );

  $gid = arg(1);
  $result0 = db_query("SELECT DISTINCT(unique_survey_id) as unique_survey_id from class_participation WHERE gid = :gid", array('gid' => $gid));
  $content .= '<div class="view-session-type"><button class="cp-list-page-button cp-list-page-button-selected" id="view_all_sessions" type="button">VIEW ALL SESSIONS</button>
      <button class="cp-list-page-button" id="view_individual_sessions" type="button">VIEW INDIVIDUAL SESSIONS</button>
      <button class="cp-list-page-button" id="view_group_sessions" type="button">VIEW GROUP SESSIONS</button></div>';
  $content .= '<div class="view-all-sessions-text">The following shows data for individual AND group sessions together.</div>';
  $content .= '<div class="view-individual-sessions-text">The following shows data for just individual sessions.</div>';
  $content .= '<div class="view-group-sessions-text">The following shows data for just group sessions.</div>';
  $content .= '<div class="control-center">' . "\n";
  $content .= t('Download Report:&nbsp;&nbsp; ');
  $options = array(
    'attributes' => array(
      'id' => 'csv-button',
      'class' => 'class-participation-csv-button button',
    ));
  $content .= l('CSV', $_GET['q'] . '/csv', $options) . ' ';
  $options['attributes']['id'] = 'print-button';
  $content .= l('PRINT', '#', $options) . ' ';
  /** Remove Access Blackboard button on 9/4/14 at request of Eric because inconsistent with rest of site **/
  // $content .= babson_forums_blackboard_link($nid);
  $content .= "\n</div>";

  $content .= '<div class="whole-table-wrapper">';

  /* VIEW ALL SESSIONS */
  unset($rows);
  $json_rows_all = array();
  $json_rows_all[] = array('name', 'score', 'comment');
  $json_rows_individual = array();
  $json_rows_individual[] = array('name', 'score', 'comment');
  $json_rows_group[] = array('name', 'score', 'comment');
  foreach ($result0 as $row0) {
    $session_title =  variable_get('class_participation_title_value_' . arg(1) . '_' . $row0->unique_survey_id);
    $result_lg_check = db_query("SELECT lgid from class_participation WHERE gid = :gid and unique_survey_id = :unique_survey_id",
      array('gid' => $gid, 'unique_survey_id' => $row0->unique_survey_id))->fetchField();
    $json_rows_all[] = array('Session:  ' . $session_title);
    if(!$result_lg_check) {
      $json_rows_individual[] = array('Session:  ' . $session_title);
    }
    if($result_lg_check) {
      $json_rows_group[] = array('Session:  ' . $session_title);
    }
    // this query excludes learning group grades.
    $lgid = null;
    $lgid = db_query('SELECT unique_survey_id from class_participation WHERE lgid is not NULL and unique_survey_id = :unique_survey_id limit 1',
      array('unique_survey_id' => $row0->unique_survey_id))->fetchField();
    $result = db_query("SELECT cp.uid, cp.unique_survey_id, cp.grade, cp.comment from class_participation cp, users u
      WHERE cp.uid = u.uid and gid = :gid and unique_survey_id = :unique_survey_id",
      array('gid' => $gid, 'unique_survey_id' => $row0->unique_survey_id));
    unset($rows);
    $max_value = variable_get('class_participation_max_value_' . $gid . '_' . $row0->unique_survey_id . '');
    if(empty($lgid)) {
      foreach ($result as $row) {
        $account = user_load($row->uid);
        $json_rows_all[] = array('name' => $account->name, 'grade' => $row->grade . ' | ' . $max_value, 'comment' => $row->comment);
        $json_rows_individual[] = array('name' => $account->name, 'grade' => $row->grade . ' | ' . $max_value, 'comment' => $row->comment);

        $rows[] = array(
          array(
            'data' => theme('username', array('account' => $account)),
            'class' => 'fake-table discussion-name'
          ),
          array(
            'data' => $row->grade . ' / ' . $max_value,
            'class' => 'fake-table total-replies grand-total-grade grade-data',

          ),
          array(
            'data' => $row->comment,
            'class' => 'fake-table total-replies grand-total-grade grade-data',

          ),
        );
      }
    }
    else {
      // this query has just learning group grades.
      $result = db_query("SELECT cp.uid, n.title as node_title, cp.grade, cp.comment, cp.lgid
          FROM class_participation cp, node n
          WHERE gid = :gid and n.nid = cp.lgid and unique_survey_id = :unique_survey_id and lgid IS NOT NULL;",
        array('gid' => $gid, 'unique_survey_id' => $row0->unique_survey_id));
      foreach ($result as $row) {
        $json_rows_all[] = array('name' => $row->node_title, 'grade' => $row->grade . ' | ' . $max_value, 'comment' => $row->comment);
        $json_rows_group[] = array('name' => $row->node_title, 'grade' => $row->grade . ' | ' . $max_value, 'comment' => $row->comment);
        $learning_group_image = "<div class='lg-image-hover'><img src='/sites/all/modules/custom/class_participation/css/images/users-lg.png' alt='lg image'>";
        $lg_users_result = db_query("select om.etid from og_membership om where om.gid = :gid and om.entity_type = 'user'",
          array('gid' => $row->lgid));
        unset($group_uid_array);
        foreach($lg_users_result as $rowlg) {
          $group_uid_array[] = $rowlg->etid;
        }
        $learning_group_bubble =
          '<div class="learning-group-id-' . $row->lgid . ' lg-row">'
          . '<div class="group-list-title">' . $row->node_title . '</div>'
          . '<div class="list-username-wrapper">';
        foreach ($group_uid_array as $lgm_uid) {
          if($lgm_uid == 1) {
            continue;
          }
          $lgm_account = user_load($lgm_uid);
          $learning_group_bubble .= '<div class="username-wrapper">'
            . theme('username', array('account' => $lgm_account)) . '</div>';
        }
        $learning_group_bubble .= '</div><!-- .list-username-wrapper --></div><!--lg-row--></div><!--lg-image-hover-->';
        $rows[] = array(
          array(
            'data' => $learning_group_image . ' ' . $learning_group_bubble . ' ' . $row->node_title,
            'class' => 'fake-table discussion-lgname'
          ),
          array(
            'data' => $row->grade . ' / ' . $max_value,
            'class' => 'fake-table total-replies grand-total-grade grade-data',
          ),
          array(
            'data' => $row->comment,
            'class' => 'fake-table total-replies grand-total-grade grade-data',
          ),
        );
      }
    }

    $session_date =  variable_get('class_participation_date_' . arg(1) . '_' . $row0->unique_survey_id);
    $content .= theme(
      'class_participation_fake_table',
      [ 'header' => $header,
        'rows' => $rows,
        'attributes' => ['class' => ['fake-data-table', 'student-display']],
        'fake_table_name' => $session_title . '<span class="session-date">' . date('m/d/Y', $session_date) . '</span>',
        'expand_collapse' => 'collapsed']
    );
    $json_for_csv_all = array('json_for_csv_all' => drupal_json_encode($json_rows_all));
    $json_for_csv_individual = array('json_for_csv_individual' => drupal_json_encode($json_rows_individual));
    $json_for_csv_group = array('json_for_csv_group' => drupal_json_encode($json_rows_group));
    drupal_add_js( array ("class_participation" => $json_for_csv_all), 'setting' );
    drupal_add_js( array ("class_participation" => $json_for_csv_individual), 'setting' );
    drupal_add_js( array ("class_participation" => $json_for_csv_group), 'setting' );
  }



  $content .= '</div><!-- End of whole-table-wrapper -->';


  $user_totals = array();
  $count = 1;
  if(isset($result)) {
    foreach($result as $row) {
      if(!isset($user_totals[$row->uid]['total'])) {
        $user_totals[$row->uid]['total'] = 0;
      }
      if(!isset($user_totals[$row->uid]['count'])) {
        $user_totals[$row->uid]['count'] = 0;
      }
      $user_totals[$row->uid]['fullname'] = $row->fullname;
      $user_totals[$row->uid]['uri'] = $row->uri;
      $user_totals[$row->uid]['total'] = $user_totals[$row->uid]['total'] + $row->grade;
      $user_totals[$row->uid]['count'] = $user_totals[$row->uid]['count'] + 1;
      $count++;
    }
  }

  // Learning Groups Grades.
  $user_totals_lg = array();
  $result = db_query('SELECT cp.lgid, cp.grade, cp.comment, n.title from class_participation cp, node n where n.nid = cp.lgid
    and gid = :gid and lgid is not null',
    array('gid' => arg(1)));
  $count = 1;
  foreach($result as $row) {
    if(!isset($user_totals_lg[$row->lgid]['total'])) {
      $user_totals_lg[$row->lgid]['total'] = 0;
    }
    if(!isset($user_totals_lg[$row->lgid]['count'])) {
      $user_totals_lg[$row->lgid]['count'] = 0;
    }
    $user_totals_lg[$row->lgid]['title'] = $row->title;
    $user_totals_lg[$row->lgid]['total'] = $user_totals_lg[$row->lgid]['total'] + $row->grade;
    $user_totals_lg[$row->lgid]['count'] = $user_totals_lg[$row->lgid]['count'] + 1;
    $count++;
  }



  $count = 1;

  foreach($user_totals as $row2) {
    if(!isset($row->fullname)) {
      $row->fullname = '';
    }
    if(!isset($row->uri)) {
      $row->uri = '';
    }

    $count++;
  }

  return $content;


}

/**
 * all for presentation layer of class-participation-list-page-by-author.
 */

function theme_class_participation_list_page_by_author($variables) {
  $cp_gid = array('cp_gid' => arg(1));
  drupal_add_js(array ("class_participation" => $cp_gid), 'setting');
  $path = drupal_get_path('module', 'class_participation');
  drupal_add_css(drupal_get_path('module', 'class_participation') . '/css/class_part_list_page.css');
  drupal_add_css("$path/css/class_participation.css");
  drupal_add_js("$path/js/class_participation_list_page.js", array('scope' => 'footer'));
  $content =  '<div class="report-types"><span class="report-type" id="by-sessions">By Sessions</span>
    <span class="report-type underlined" id="by-students">By Students</span></div>';
  $node = node_load(arg(1));
  drupal_set_title($node->title . ' : ' . 'Class Participation');

  $header_totals = array(
    array(
      'data' => t(''),
      'class' => 'fake-table discussion-name-totals'
    ),
    array(
      'data' => t('Total Sessions'),
      'class' => 'fake-table discussion-name-totals'
    ),
    array(
      'data' => t('Total Individual Sessions'),
      'class' => 'fake-table discussion-name-totals'
    ),
    array(
      'data' => t('Total Group Sessions'),
      'class' => 'fake-table discussion-name-totals'
    ),
    array(
      'data' => t('Total Scores'),
      'class' => 'fake-table discussion-name-totals'
    ),

  );

  $header = array(
    array(
      'data' => t('Name'),
      'class' => 'fake-table discussion-name'
    ),
    array(
      'data' => t('Score'),
      'class' => 'fake-table total-replies grand-total-grade grade-data',
      'data-hide' => 'phone,tablet'
    ),
    array(
      'data' => t('Comment'),
      'class' => 'fake-table total-replies grand-total-grade grade-data',
      'data-hide' => 'phone,tablet'
    ),
  );

  $content .= '<div class="student-controls">';
  $render = drupal_get_form('class_participation_class_members_form');
  $content .= drupal_render($render);
  $content .= '<div class="left-right-buttons"><div class="row1 button cc-primary-button left-button">Previous</div><div class="row2 button cc-primary-button right-button">Next</div></div>';
  $content .= '</div>';
  $content .= '<div class="view-session-type"><button class="cp-list-page-button cp-list-page-button-selected" id="view_all_sessions" type="button">VIEW ALL SESSIONS</button>
      <button class="cp-list-page-button" id="view_individual_sessions" type="button">VIEW INDIVIDUAL SESSIONS</button>
      <button class="cp-list-page-button" id="view_group_sessions" type="button">VIEW GROUP SESSIONS</button></div>';
  $content .= '<div class="view-all-sessions-text">The following shows data for individual AND group sessions together.</div>';
  $content .= '<div class="view-individual-sessions-text">The following shows data for just individual sessions.</div>';
  $content .= '<div class="view-group-sessions-text">The following shows data for just group sessions.</div>';
  $content .= '<div class="control-center-list-page-by-author">' . "\n";
  $content .= t('Download Report: &nbsp;&nbsp;');
  $options = array(
    'attributes' => array(
      'id' => 'csv-button',
      'class' => 'class-participation-csv-button button',
    ));
  $content .= l('CSV', $_GET['q'] . '/csv', $options) . ' ';
  $options['attributes']['id'] = 'print-button';
  $content .= l('PRINT', '#', $options) . ' ';
  /** Remove Access Blackboard button on 9/4/14 at request of Eric because inconsistent with rest of site **/
  // $content .= babson_forums_blackboard_link($nid);
  $content .= "\n</div>";
  $arg3 = arg(3);
  if (isset($arg3)) {
    $selected_user_uid = arg(3);
  }
  else{
    $sql_class_users = "SELECT DISTINCT(u.uid) AS uid
      FROM users u
      JOIN og_membership ogm ON u.uid = ogm.etid AND ogm.entity_type = 'user' AND ogm.gid = :gid
      INNER JOIN field_data_field_last_name ln ON u.uid = ln.entity_id
      INNER JOIN field_data_field_first_name fn ON u.uid = fn.entity_id
      WHERE u.status = 1
      AND ogm.state = 1
    ";
    $first_user_uid = db_query($sql_class_users, array('gid' => arg(1)))->fetchField();
    $selected_user_uid = $first_user_uid;
  }

  $gid = arg(1);
  $result0 = db_query("SELECT distinct(unique_survey_id) as unique_survey_id from class_participation WHERE gid = :gid", array('gid' => $gid));
  $result1 = db_query("SELECT distinct(unique_survey_id) as unique_survey_id from class_participation WHERE gid = :gid", array('gid' => $gid));
  $result0_only_lgid = db_query("SELECT distinct(unique_survey_id) as unique_survey_id from class_participation WHERE gid = :gid and lgid is not null", array('gid' => $gid));
  $total_sessions = $result0->rowCount();
  $total_sessions_lgid = $result0_only_lgid->rowCount();
  $total_user_score_individual = db_query('SELECT sum(grade) from class_participation where uid = :uid', array('uid' => $selected_user_uid))->fetchField();
  $total_lg_scores_for_individual = db_query("select om.gid, cp.grade from og_membership om, class_participation cp where cp.lgid = om.gid and om.entity_type = 'user'
    and om.etid = :uid AND om.gid in (select lgid from class_participation cp where gid = :gid)", array('uid' => $selected_user_uid, 'gid' => $gid));
  $total_lg_score = 0;
  foreach ($total_lg_scores_for_individual as $lg_score) {
    $total_lg_score = $total_lg_score + $lg_score->grade;
  }
  $total_user_score = $total_user_score_individual + $total_lg_score;
  $content .= '<div class="whole-table-wrapper">';
  $json_rows_all = array();
  $json_rows_all[] = array('last_name' => 'Last Name', 'first_name' => 'First Name', 'Username' => 'Username', 'grade' => 'Grade', 'comment' => 'Comment');
  $json_rows_individual = array();
  $json_rows_individual[] = array('last_name' => 'Last Name', 'first_name' => 'First Name', 'Username' => 'Username', 'grade' => 'Grade', 'comment' => 'Comment');
  $json_rows_group = array();
  $json_rows_group[] = array('last_name' => 'Last Name', 'first_name' => 'First Name', 'Username' => 'Username', 'grade' => 'Grade', 'comment' => 'Comment');

  $total_max_value = 0;
  foreach ($result1 as $roww) {
    $max_value = variable_get('class_participation_max_value_' . $gid . '_' . $roww->unique_survey_id . '');
    $total_max_value = $total_max_value + $max_value;
  }
  $rows_totals[] = array(
    array(
      'data' => 'Totals',
      'class' => 'fake-table course-total-grade course-total-grade-left'
    ),
    array(
      'data' => $total_sessions,
      'class' => 'fake-table course-total-grade',

    ),
    array(
      'data' => $total_sessions - $total_sessions_lgid,
      'class' => 'fake-table course-total-grade',

    ),
    array(
      'data' => $total_sessions_lgid,
      'class' => 'fake-table course-total-grade',

    ),
    array(
      'data' =>  $total_user_score . " / " . $total_max_value,
      'class' => 'fake-table course-total-grade',

    ),
  );

  if (!isset($arg3)) {
    $sql_class_users = "SELECT DISTINCT(u.uid) AS uid, u.picture AS users_picture,
      u.name AS users_name, field_first_name_value AS first_name, field_last_name_value AS last_name,
      CONCAT(fn.field_first_name_value, ' ', ln.field_last_name_value) as fullname
      FROM users u
      JOIN og_membership ogm ON u.uid = ogm.etid AND ogm.entity_type = 'user' AND ogm.gid = :gid
      INNER JOIN field_data_field_last_name ln ON u.uid = ln.entity_id
      INNER JOIN field_data_field_first_name fn ON u.uid = fn.entity_id
      WHERE u.status = 1
      AND ogm.state = 1
      GROUP BY uid
      ORDER BY last_name, first_name limit 1
    ";
    $selected_user_uid = db_query($sql_class_users, array('gid' => arg(1)))->fetchField();
  }

  $content .= theme(
    'class_participation_fake_table',
    [ 'header' => $header_totals,
      'rows' => $rows_totals,
      'attributes' => ['class' => ['fake-data-table', 'student-display']],
      'fake_table_name' => 'Course Totals',
      'expand_collapse' => 'collapsed']
  );
  foreach ($result0 as $row0) {
    $session_title =  variable_get('class_participation_title_value_' . arg(1) . '_' . $row0->unique_survey_id);
    $result_lg_check = db_query("SELECT lgid from class_participation WHERE gid = :gid and unique_survey_id = :unique_survey_id",
      array('gid' => $gid, 'unique_survey_id' => $row0->unique_survey_id))->fetchField();
    $json_rows_all[] = array('Session:  ' . $session_title);
    if(!$result_lg_check) {
      $json_rows_individual[] = array('Session:  ' . $session_title);
    }
    if($result_lg_check) {
      $json_rows_group[] = array('Session:  ' . $session_title);
    }

    $result = db_query("SELECT cp.uid, cp.grade, cp.comment from class_participation cp, users u
      WHERE cp.uid = u.uid and gid = :gid and u.uid = :first_user_uid and cp.unique_survey_id = :unique_survey_id",
      array('gid' => $gid, 'first_user_uid' => $selected_user_uid, 'unique_survey_id' => $row0->unique_survey_id));
    unset($rows);
    $max_value = variable_get('class_participation_max_value_' . $gid . '_' . $row0->unique_survey_id . '');
    foreach ($result as $row) {
      $account = user_load($row->uid);
      $json_rows_all[] = array('name' => $account->name, 'grade' => $row->grade, 'comment' => $row->comment);
      $json_rows_individual[] = array('name' => $account->name, 'grade' => $row->grade, 'comment' => $row->comment);


      $rows[] = array(
        array(
          'data' => theme('username', array('account' => $account)),
          'class' => 'fake-table discussion-name'
        ),
        array(
          'data' => $row->grade . ' / ' . $max_value,
          'class' => 'fake-table total-replies grand-total-grade grade-data',

        ),
        array(
          'data' => $row->comment,
          'class' => 'fake-table total-replies grand-total-grade grade-data',

        ),
      );
    }
    $session_title =  variable_get('class_participation_title_value_' . arg(1) . '_' . $row0->unique_survey_id);
    $session_date =  variable_get('class_participation_date_' . arg(1) . '_' . $row0->unique_survey_id);

    // this query has just learning group grades.
    $result = db_query("SELECT cp.uid, n.title as node_title, cp.grade, cp.comment, cp.lgid
          FROM class_participation cp, node n
          WHERE gid = :gid and n.nid = cp.lgid and unique_survey_id = :unique_survey_id and lgid IS NOT NULL
          AND :uid in (SELECT om.etid from og_membership om where om.gid = :gid);",
      array('gid' => $gid, 'unique_survey_id' => $row0->unique_survey_id, 'uid' => $selected_user_uid));
    foreach ($result as $row) {
      $json_rows_all[] = array('name' => $row->node_title, 'grade' => $row->grade . ' | ' . $max_value, 'comment' => $row->comment);
      $json_rows_group[] = array('name' => $row->node_title, 'grade' => $row->grade . ' | ' . $max_value, 'comment' => $row->comment);
      $is_group_member = null;
      $is_group_member = db_query("SELECT * from og_membership om where om.gid = :lgid and om.entity_type = 'user' and om.etid = :user",
        array('lgid' => $row->lgid, 'user' => $selected_user_uid))->fetchField();
      if(empty($is_group_member)) {
        continue;
      }
      $learning_group_image = "<div class='lg-image-hover'><img src='/sites/all/modules/custom/class_participation/css/images/users-lg.png' alt='lg image'>";
      $lg_users_result = db_query("select om.etid from og_membership om where om.gid = :gid and om.entity_type = 'user'",
        array('gid' => $row->lgid));
      unset($group_uid_array);
      foreach($lg_users_result as $rowlg) {
        $group_uid_array[] = $rowlg->etid;
      }
      $learning_group_bubble =
        '<div class="learning-group-id-' . $row->lgid . ' lg-row">'
        . '<div class="group-list-title">' . $row->node_title . '</div>'
        . '<div class="list-username-wrapper">';
      foreach ($group_uid_array as $lgm_uid) {
        $lgm_account = user_load($lgm_uid);
        if($lgm_uid == 1) {
          continue;
        }
        $learning_group_bubble .= '<div class="username-wrapper">'
          . theme('username', array('account' => $lgm_account)) . '</div>';
      }
      $learning_group_bubble .= '</div><!-- .list-username-wrapper --></div><!--lg-row--></div><!--lg-image-hover-->';
      $rows[] = array(
        array(
          'data' => $learning_group_image . ' ' . $learning_group_bubble . ' ' . $row->node_title,
          'class' => 'fake-table discussion-lgname',
        ),
        array(
          'data' => $row->grade . ' / ' . $max_value,
          'class' => 'fake-table total-replies grand-total-grade grade-data',

        ),
        array(
          'data' => $row->comment,
          'class' => 'fake-table total-replies grand-total-grade grade-data',

        ),
      );
    }
    if(empty($rows)) {
      $rows = null;
    }
    if(count($rows) == 0) {
      continue;
    }
    $content .= theme(
      'class_participation_fake_table',
      [ 'header' => $header,
        'rows' => $rows,
        'attributes' => ['class' => ['fake-data-table', 'student-display']],
        'fake_table_name' => $session_title . '<span class="session-date">' . date('m/d/Y', $session_date) . '</span>',
        'expand_collapse' => 'collapsed']
    );
    $json_for_csv_all = array('json_for_csv_all' => drupal_json_encode($json_rows_all));
    drupal_add_js( array ("class_participation" => $json_for_csv_all), 'setting' );
    $json_for_csv_individual = array('json_for_csv_individual' => drupal_json_encode($json_rows_individual));
    drupal_add_js( array ("class_participation" => $json_for_csv_individual), 'setting' );
    $json_for_csv_group = array('json_for_csv_group' => drupal_json_encode($json_rows_group));
    drupal_add_js( array ("class_participation" => $json_for_csv_group), 'setting' );
  }
  $content .= '</div><!-- End of whole-table-wrapper -->';
  return $content;
}

/**
 * all for presentation layer of class-participation-list-page-by-author.
 */

function theme_class_participation_enter_grades($variables) {
  drupal_add_library('system', 'ui.datepicker');

  $node = node_load(arg(1));
  $content = '<div class="page-header page-header-expand">Configuration for Class ' . $node->title . ' Participation Grades</div>';
  $form1 = drupal_get_form('class_participation_custom_form_config2');
  $content .= drupal_render($form1);
  $form2 = drupal_get_form('class_participation_custom_form_config');
  $content .= drupal_render($form2);
  return $content;
}